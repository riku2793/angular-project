import { Injectable } from '@angular/core';
import { Recipe } from '../recipe/recipe';
import { RECIPEDATA } from '../recipe/recipedata';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipeData: Recipe[] = RECIPEDATA;

  currentValue = '';

  constructor() {
  }

  /**
   * レシピの全データを返す
   */
  getRecipeData(): Recipe[] {
    return this.recipeData;
  }

  /**
   * 指定したidのデータを返す
   * @param id // レシピのid
   */
  getRecipe(id: string): Recipe {
    return this.recipeData.find(recipe => recipe.id.toString() === id);
  }

  /**
   * keywordを含むレシピを返す
   * @param keyword // 検索キーワード
   */
  searchRecipe(keyword: string): Recipe[] {
    this.currentValue = keyword;
    return this.recipeData.filter(recipe => JSON.stringify(recipe).search(keyword) >= 0);
  }

  /**
   * ソートしたレシピを返す
   * @param sortType // ソートの種類
   */
  sortRecipe(sortType: string, asc_or_des: string): Recipe[] {
    switch (sortType) {
      case '名前':
        return asc_or_des === '昇順' ?
          this.recipeData.sort((recipe1, recipe2) => recipe1.name > recipe2.name ? 1 : -1) :
          this.recipeData.sort((recipe1, recipe2) => recipe1.name > recipe2.name ? -1 : 1);
      case '調理時間':
        return asc_or_des === '昇順' ?
          this.recipeData.sort((recipe1, recipe2) => recipe1.minute > recipe2.minute ? 1 : -1) :
          this.recipeData.sort((recipe1, recipe2) => recipe1.minute > recipe2.minute ? -1 : 1);

    }
  }
}
