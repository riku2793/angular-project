import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Recipe } from '../recipe/recipe';
import { RecipeService } from '../Service/recipe.service';

@Component({
  selector: 'app-recipe-data',
  templateUrl: './recipe-data.component.html',
  styleUrls: ['./recipe-data.component.css']
})
export class RecipeDataComponent implements OnInit {
  recipeId: string;
  recipe: Recipe;
  constructor(private route: ActivatedRoute, private location: Location,
              private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipeId = this.route.snapshot.paramMap.get('id');
    this.recipe = this.recipeService.getRecipe(this.recipeId);
  }

  /**
   * 前のページに戻る
   */
  backToList() {
    this.location.back();
  }

}
