import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {
  editRecipeForm: FormGroup;
  minuteSel = [10, 20, 30, 40, 50, 60];
  serveSel = [1, 2, 3, 4];
  constructor(private location: Location, private fb: FormBuilder) {
    this.editRecipeForm = this.fb.group({
      name: '編集前の料理名',
      feature: '編集前のひとこと',
      minute: this.minuteSel[0],
      ingre: '編集前の材料',
      serve: this.serveSel[0]
    });
  }

  ngOnInit() {
  }

  backToRecipeData() {
    this.location.back();
  }
}
