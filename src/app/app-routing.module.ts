import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeDataComponent } from './recipe-data/recipe-data.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { EditRecipeComponent } from './edit-recipe/edit-recipe.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/recipe-list', pathMatch: 'full' },
      { path: 'recipe-list', component: RecipeListComponent },
      { path: 'recipe-data/:id', component: RecipeDataComponent },
      { path: 'add-recipe', component: AddRecipeComponent },
      { path: 'edit-recipe', component: EditRecipeComponent }
    ])
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
