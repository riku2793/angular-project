import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {
  addRecipeForm: FormGroup;
  minuteSel = [10, 20, 30, 40, 50, 60];
  serveSel = [1, 2, 3, 4];
  constructor(private location: Location, private fb: FormBuilder) {
    this.addRecipeForm = this.fb.group({
      name: '',
      feature: '',
      minute: this.minuteSel[0],
      ingre: '',
      serve: this.serveSel[0]
    });
  }

  ngOnInit() {
  }

  backToList() {
    this.location.back();
  }

}
