import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe/recipe';
import { RecipeService } from '../Service/recipe.service';
import { FormGroup, FormControl, FormBuilder, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipeData: Recipe[];
  sortTypeSel = ['名前', '調理時間'];
  asc_or_desSel = ['昇順', '降順'];
  sortForm: FormGroup;
  asc_or_desText = '昇順';
  constructor(private recipeService: RecipeService, private fb: FormBuilder) {
    this.sortForm = this.fb.group({
      sortType: this.sortTypeSel[0],
      asc_or_des: this.asc_or_desSel[0]
    });
  }

  ngOnInit() {
    this.recipeData = this.recipeService.getRecipeData();
  }

  /**
   * ソートを行う
   * @param event
   */
  sortRecipe(sortType: string) {
    this.recipeData = this.recipeService.sortRecipe(sortType, this.asc_or_desText);
  }

  /**
   * キーワードを含むレシピを返す
   * @param keyword // 検索キーワード
   */
  searchRecipe(keyword: string) {
    this.recipeData = this.recipeService.searchRecipe(keyword);
  }

  /**
   * 検索窓に入力されている値を返す
   */
  getCurrentValue() {
    return this.recipeService.currentValue;
  }

  /**
   * 昇順,降順の切り替え
   */
  changeAsc_or_des() {
    this.asc_or_desText = this.asc_or_desText === '昇順' ? '降順' : '昇順';
    this.recipeData = this.recipeData.reverse();
  }
}
